#pragma once

#include <string>

#ifndef BOOKINFO_H
#define BOOKINFO_H

enum BookInfo
{
  AUTHOR,
  TITLE,
  SUBTITLE,
  YEAR,
  IS_FICTION,
  STATUS,
  SERIES,
  SUBGENRE,
  RECOMMENDATION
};

std::string
book_info_to_string(BookInfo bi);

#endif // BOOKINFO_H
