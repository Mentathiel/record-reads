#include "Book.h"

using namespace std;

Book::Book(vector<string> authors, string title, int year_of_publication,
           bool is_fiction, Status status, string subtitle /* = "" */,
           string series /* = "" */, string comment /* = "" */,
           vector<string> subgenres /* = vector<string>() */,
           Recommendation recommendation /* = NEUTRAL */) {
  m_authors             = authors;
  m_title               = title;
  m_year_of_publication = year_of_publication;
  m_is_fiction          = is_fiction;
  m_status              = status;
  m_sub_title           = subtitle;
  m_series              = series;
  m_comment             = comment;
  m_subgenres           = vector<string>(subgenres);
  m_recommendation      = recommendation;
}

string
Book::pretty_print() {
  string authors = "";
  for (auto &element : m_authors) {
    authors += element + ";";
  }

  string subgenres = "";
  for (auto &element : m_subgenres) {
    subgenres += element + ";";
  }

  string to_return = "Author(s): " + authors + "\n";
  to_return += "Title: " + m_title + "\n";

  if (m_sub_title != "") {
    to_return += "Subtitle: " + m_sub_title + "\n";
  }

  if (m_series != "") {
    to_return += "Series: " + m_series + "\n";
  }

  to_return += "Year: " + to_string(m_year_of_publication) + "\n";
  string is_fiction = m_is_fiction ? "Yes" : "No";
  to_return += "Fiction: " + is_fiction + "\n";
  to_return += "Status: " + status_to_string(m_status) + "\n";

  if (m_comment != "") {
    to_return += "Comment: " + m_comment + "\n";
  }

  if (subgenres != "") {
    to_return += "Subgenres: " + subgenres + "\n";
  }

  if (m_recommendation != NEUTRAL) {
    to_return +=
        "Recommendation: " + recommendation_to_string(m_recommendation) + "\n";
  }

  return to_return;
}

// getters
vector<string>
Book::get_authors() {
  return m_authors;
}

string
Book::get_title() {
  return m_title;
}

int
Book::get_year_of_publication() {
  return m_year_of_publication;
}

bool
Book::is_fiction() {
  return m_is_fiction;
}

Status
Book::get_status() {
  return m_status;
}

string
Book::get_sub_title() {
  return m_sub_title;
}

string
Book::get_series() {
  return m_series;
}

string
Book::get_comment() {
  return m_comment;
}

vector<string>
Book::get_subgenres() {
  return m_subgenres;
}

Recommendation
Book::get_recommendation() {
  return m_recommendation;
}
