#pragma once

#ifndef STATUS_H
#define STATUS_H

#include <string>

enum Status
{
  TO_READ,
  ALREADY_READ,
  DROPPED,
  READING,
  TO_EVALUATE,
  STATUS_COUNT
};

std::string
status_to_string(Status status);

#endif // STATUS_H
