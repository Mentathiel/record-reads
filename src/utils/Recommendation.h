#pragma once

#ifndef RCMND_H
#define RCMND_H

#include <string>
enum Recommendation
{
  RECOMMENDED,
  NOT_RECOMMENDED,
  NEUTRAL,
  RECOMMENDATION_COUNT
};

std::string
recommendation_to_string(Recommendation recommendation);

#endif // RCMND_H
