#pragma once

#include "Book.h"
#include "BookInfo.h"

#include <iostream>
#include <string>
#include <vector>

#include <QApplication>
#include <QFile>
#include <QList>
#include <QTextStream>
#include <QVector>

#ifndef BOOKCOLLECTION_H
#define BOOKCOLLECTION_H

class BookCollection {
public:
  BookCollection();

  void add(Book book);
  bool contains(Book book);

  // Function that generates a test book collection by reading the first
  // _numberOfBooksToRead lines from a .csv file
  void readFromCsv(int _numberOfBooksToRead);

  template <typename T> void remove(BookInfo field, T value);

  void sort(std::vector<BookInfo> criteria);

  template <typename T> void apply_filter(BookInfo field, T value);

  void reset_filters();

  std::vector<Book> get_filtered_books();

  std::vector<Book> get_all_books();

private:
  QVector<Book> m_books;
  QVector<Book> m_filtered_books;
};

#endif // BOOKCOLLECTION_H
