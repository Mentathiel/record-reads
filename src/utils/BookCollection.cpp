#include "BookCollection.h"

BookCollection::BookCollection() {
  m_books          = QVector<Book>();
  m_filtered_books = QVector<Book>();
  readFromCsv(10);
}

void
BookCollection::add(Book book) {
  m_books.append(book);
}

void
BookCollection::readFromCsv(int _numberOfBooksToRead) {
  QFile file(":/MyData/utils/data-assets/books.csv");
  QStringList firstRow;
  if (!file.open(QIODevice::ReadOnly)) {
    return;
  }

  QTextStream s1(&file);
  int booksRead = 0;
  // Skip the header row
  QString s = s1.readLine();
  while (!s1.atEnd() && booksRead < _numberOfBooksToRead) {
    s                      = s1.readLine();
    QStringList currentRow = s.split(",");
    if (currentRow.length() < 5) {
      continue;
    }
    std::vector<std::string> authors =
        std::vector<std::string>({currentRow[0].toStdString()});
    std::string title = currentRow[1].toStdString();
    int year          = currentRow[2].trimmed().toInt();
    bool isFiction    = true;
    Status status     = Status::TO_READ;

    Book bookToAdd = Book(authors, title, year, isFiction, status);
    this->add(bookToAdd);
    booksRead++;
  }
}
