#include "BookInfo.h"

std::string
book_info_to_string(BookInfo bi) {
  switch (bi) {
  case BookInfo::AUTHOR:
    return "Author";
  case BookInfo::TITLE:
    return "Title";
  case BookInfo::SUBTITLE:
    return "Subtitle";
  case BookInfo::YEAR:
    return "Year of publication";
  case BookInfo::IS_FICTION:
    return "Is fiction?";
  case BookInfo::STATUS:
    return "Status";
  case BookInfo::SERIES:
    return "Series";
  case BookInfo::SUBGENRE:
    return "Subgenres";
  case BookInfo::RECOMMENDATION:
    return "Recommendation";
  default:
    return "";
  }
}
