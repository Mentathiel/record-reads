#pragma once

#ifndef BOOK_H
#define BOOK_H

#include "Recommendation.h"
#include "Status.h"
#include <string>
#include <vector>

class Book {
private:
  // required fields
  std::vector<std::string> m_authors;
  std::string m_title       = "";
  int m_year_of_publication = -1;
  bool m_is_fiction;
  Status m_status;

  // optional fields
  // for books with convoluted names
  std::string m_sub_title = "";
  // contains the name of the series the book belongs to if there is one
  std::string m_series = "";
  // contains a comment about the book, if there is one
  std::string m_comment = "";
  // sub genres of the book (fiction/non fiction is not optional and
  // is in a separate value)
  std::vector<std::string> m_subgenres;
  // only used when a book is heavily (not) recommended, Neutral otherwise
  Recommendation m_recommendation;

public:
  Book(std::vector<std::string> authors, std::string title,
       int year_of_publication, bool is_fiction, Status status,
       std::string subtitle = "", std::string series = "",
       std::string comment                = "",
       std::vector<std::string> subgenres = std::vector<std::string>(),
       Recommendation recommendation      = NEUTRAL);

  std::string pretty_print();

  // getters
  std::vector<std::string> get_authors();

  std::string get_title();

  int get_year_of_publication();

  bool is_fiction();

  Status get_status();

  std::string get_sub_title();

  std::string get_series();

  std::string get_comment();

  std::vector<std::string> get_subgenres();

  Recommendation get_recommendation();
};

#endif // BOOK_H
