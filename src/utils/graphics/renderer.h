#ifndef RENDERER_H
#define RENDERER_H

#include "../Book.h"
#include "../Status.h"

#include <QFont>
#include <QFontDialog>
#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <QGraphicsTextItem>
#include <QGraphicsView>
#include <QLayout>

class Renderer {
public:
  Renderer(QGraphicsScene *_scene);
  QFont selectNewFont();
  void drawBookInScene(Book &_book);
  QBrush getBookBrush(Book _book);
  void changeFill(QGraphicsRectItem _item);
  void clearScene();

private:
  QFont m_titleFont;
  QGraphicsScene *m_scene;
};

#endif // RENDERER_H
