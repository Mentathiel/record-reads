#ifndef GRAPHWIDGET_HPP
#define GRAPHWIDGET_HPP

#include "booknode.h"

#include <QKeyEvent>
#include <QLayout>
#include <QPainter>
#include <QTimerEvent>
#include <QtMath>
#include <qgraphicsview.h>

class GraphWidget : public QGraphicsView {
  Q_OBJECT

public:
  GraphWidget(QWidget *parent = nullptr);

  void itemMoved();

protected:
  void keyPressEvent(QKeyEvent *event) override;
  void timerEvent(QTimerEvent *event) override;
#if QT_CONFIG(wheelevent)
  void wheelEvent(QWheelEvent *event) override;
#endif
  void drawBackground(QPainter *painter, const QRectF &rect) override;

  void scaleView(qreal scaleFactor);

private:
  int timerId = 0;
  BookNode *m_centerNode;
};

#endif // GRAPHWIDGET_HPP
