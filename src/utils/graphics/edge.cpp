#include "edge.h"

Edge::Edge(BookNode *_src, BookNode *_dest) : m_src(_src), m_dest(_dest) {

  m_src->addEdge(this);
  m_dest->addEdge(this);
  adjust();
}

BookNode *
Edge::sourceNode() const {
  return m_src;
}

BookNode *
Edge::destNode() const {
  return m_dest;
}

void
Edge::adjust() {
  if (!m_src || !m_dest)
    return;

  // A line that connects the two BookNodes, in item coordinates
  // Calculated using the individual Nodes' origins cast to this item's
  // coordinates
  QLineF line(mapFromItem(m_src, 0, 0), mapFromItem(m_dest, 0, 0));
  qreal length = line.length();

  prepareGeometryChange();

  // This is just in case the nodes overlap, even though the nodes' positions
  // are fixed
  // TODO: Extract node size into a GraphicsConstants class of some sort
  if (length > qreal(20.)) {
    QPointF edgeOffset((line.dx() * 10) / length, (line.dy() * 10) / length);
    m_srcPoint  = line.p1() + edgeOffset;
    m_destPoint = line.p2() - edgeOffset;
  } else {
    m_srcPoint = m_destPoint = line.p1();
  }
}

QRectF
Edge::boundingRect() const {
  if (!m_src || !m_dest)
    return QRectF();

  // To define the bounding rectangle of a line we need to set it
  // to the smallest rectangle that will contain said line, which
  // in this case is the rectangle whose diagonal is said line
  // TODO: Add an adjustment step if a dedicated "add node" button is
  // implemented
  return QRectF(m_srcPoint, QSizeF(m_destPoint.x() - m_srcPoint.x(),
                                   m_destPoint.y() - m_srcPoint.y()))
      .normalized();
}

void
Edge::paint(QPainter *_painter, const QStyleOptionGraphicsItem *_option,
            QWidget *_widget) {
  if (!m_src || !m_dest)
    return;

  QLineF line(m_srcPoint, m_destPoint);

  // This basically just approximates if the length of the line is 0
  // A bit overkill for its intended use
  if (qFuzzyCompare(line.length(), qreal(0.)))
    return;

  _painter->setPen(
      QPen(Qt::gray, 2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
  _painter->drawLine(line);
}
