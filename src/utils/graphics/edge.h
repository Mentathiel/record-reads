#ifndef EDGENODE_H
#define EDGENODE_H

#include "booknode.h"
#include <QGraphicsItem>
#include <QLineF>
#include <QVector>

class BookNode;

class Edge : public QGraphicsItem {
public:
  Edge(BookNode *_src, BookNode *_dest);
  BookNode *sourceNode() const;
  BookNode *destNode() const;

  // Function that adjusts the m_srcPoint and m_destPoint
  // using the coordinates of the BookNodes. Albeit a bit
  // overkill for fixed nodes, but useful in case the nodes
  // moves in future versions.
  void adjust();

protected:
  // QGraphicsItem overrided functions used for drawing
  QRectF boundingRect() const;
  void paint(QPainter *_painter, const QStyleOptionGraphicsItem *_option,
             QWidget *_widget);

private:
  // Pointers to BookNodes which the edge connects
  BookNode *m_src;
  BookNode *m_dest;

  // Current location of source and destination nodes
  // Used for drawing our the edge itself
  QPointF m_srcPoint;
  QPointF m_destPoint;

public:
};

#endif // EDGENODE_H
