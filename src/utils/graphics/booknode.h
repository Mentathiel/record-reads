#ifndef BOOKNODE_H
#define BOOKNODE_H

#include "../Book.h"
#include "edge.h"
#include <QGraphicsItem>
#include <QGraphicsView>
#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QWidget>
#include <src/utils/BookCollection.h>

class Edge;

class BookNode : public QGraphicsItem {
public:
  BookNode(BookCollection *_bookCollection, QGraphicsView *_graph);
  BookNode(QGraphicsView *_graph);
  void addEdge(Edge *_edge);
  QVector<Edge *> edges() const;

  // QGraphicsItem overrided functions used for drawing and collision detection
  // (if needed)
  QRectF boundingRect() const override;
  QPainterPath shape() const override;
  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
             QWidget *widget) override;

protected:
  void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
  void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;

private:
  BookCollection *m_bookCollection;
  QVector<Edge *> m_edgeList;
  QGraphicsView *m_graph;
};

#endif // BOOKNODE_H
