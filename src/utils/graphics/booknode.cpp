#include "booknode.h"

BookNode::BookNode(BookCollection *_bookCollection, QGraphicsView *_graph)
    : m_bookCollection(_bookCollection), m_graph(_graph) {
  // Flag that allows the item to move in response to user input
  // In our case, for example, clicking the zoom button
  setFlag(ItemIsMovable);
  setFlag(ItemSendsGeometryChanges);

  // Cache mode which is optimized for items of a fixed size
  // but can move
  setCacheMode(DeviceCoordinateCache);
  setZValue(-1);
}

BookNode::BookNode(QGraphicsView *_graph) : m_graph(_graph) {
  // Flag that allows the item to move in response to user input
  // In our case, for example, clicking the zoom button
  setFlag(ItemIsMovable);
  setFlag(ItemSendsGeometryChanges);

  // Cache mode which is optimized for items of a fixed size
  // but can move
  setCacheMode(DeviceCoordinateCache);
  setZValue(-1);
  m_bookCollection = new BookCollection();
}

void
BookNode::addEdge(Edge *_edge) {
  m_edgeList.append(_edge);
  _edge->adjust();
}

QVector<Edge *>
BookNode::edges() const {
  return m_edgeList;
}

QRectF
BookNode::boundingRect() const {
  qreal deltaStroke = 2;
  return QRectF(-10 - deltaStroke, -10 - deltaStroke, 23 + deltaStroke,
                23 + deltaStroke);
}

QPainterPath
BookNode::shape() const {
  QPainterPath path;
  path.addEllipse(-10, -10, 20, 20);
  return path;
}

void
BookNode::paint(QPainter *_painter, const QStyleOptionGraphicsItem *_option,
                QWidget *_widget) {

  // Change the fill(brush) color to a darker shade if the node is clicked
  QBrush brush(Qt::cyan);
  if (_option->state & QStyle::State_Sunken) {
    QBrush brush(Qt::darkCyan);
  }

  _painter->setBrush(brush);

  _painter->setPen(QPen(Qt::darkCyan, 0));
  _painter->drawEllipse(-10, -10, 20, 20);
}

void
BookNode::mousePressEvent(QGraphicsSceneMouseEvent *event) {
  update();
  QGraphicsItem::mousePressEvent(event);
}

void
BookNode::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
  update();
  QGraphicsItem::mouseReleaseEvent(event);
}
