#include "renderer.h"

Renderer::Renderer(QGraphicsScene *_scene) : m_scene(_scene) {
  this->m_titleFont = QFont("Times", 10, QFont::Bold);
  selectNewFont();
}
QFont
Renderer::selectNewFont() {
  bool ok;
  this->m_titleFont = QFontDialog::getFont(&ok);

  return this->m_titleFont;
}
void
Renderer::changeFill(QGraphicsRectItem _item) {

  _item.setBrush(Qt::red);
}

void
Renderer::clearScene() {
  this->m_scene->clear();
}
void
Renderer::drawBookInScene(Book &_book) {
  // Extracting book info
  QString title = QString::fromStdString(_book.get_title());

  // Generating frame for book visual representation
  QGraphicsRectItem *frame = new QGraphicsRectItem();

  // Frame formatting
  frame->setRect(0, 0, 200, 100);
  frame->setBrush(getBookBrush(_book));

  // Inserting text into graphics item
  /* The linter seems to think that it isn't using this textItem
   * even though it displays the desired text bevause the text
   * object's parent is the book frame
   */
  QGraphicsTextItem *textItem = new QGraphicsTextItem(title, frame);

  // Text formatting
  textItem->adjustSize();
  textItem->setFont(m_titleFont);

  // Adding item to scene
  m_scene->addItem(frame);

  return;
}

QBrush
Renderer::getBookBrush(Book _book) {
  if (_book.is_fiction()) {
    return Qt::red;
  } else {
    return Qt::blue;
  }
}
