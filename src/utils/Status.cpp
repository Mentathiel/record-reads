#include "Status.h"

using namespace std;

string
status_to_string(Status status) {
  switch (status) {
  case Status::TO_READ:
    return "To read";
  case Status::ALREADY_READ:
    return "Already Read";
  case Status::DROPPED:
    return "Dropped";
  case Status::READING:
    return "Reading";
  case Status::TO_EVALUATE:
    return "Evaluating";
  default:
    return "";
  }
}
