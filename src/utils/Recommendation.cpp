#include "Recommendation.h"

using namespace std;

string
recommendation_to_string(Recommendation recommendation) {
  switch (recommendation) {
  case Recommendation::RECOMMENDED:
    return "Recommended";
  case Recommendation::NOT_RECOMMENDED:
    return "Not recommended";
  case Recommendation::NEUTRAL:
    return "Neutral";
  default:
    return "";
  }
}
