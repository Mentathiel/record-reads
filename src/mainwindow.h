#pragma once

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "./utils/Book.h"

#include <QGraphicsScene>
#include <QMainWindow>

#include <src/utils/graphics/graphwidget.h>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

  void SanityCheck(QGraphicsScene *_scene);
  void DrawBookInScene(Book &_book, QGraphicsScene *_scene);

private slots:
  void on_pushButtonAddBook_clicked();

  void on_pushButtonRemoveBook_clicked();

  void on_pushButtonEditGenres_clicked();

  void on_pushButtonEditAuthors_clicked();

  void on_toolButtonGenreAdd_clicked();

  void on_toolButtonRemoveAllGenre_clicked();

  void on_toolButtonRemoveSelGenre_clicked();

  void on_toolButtonAuthorAdd_clicked();

  void on_toolButtonRemoveSelAuthors_clicked();

  void on_toolButtonRemoveAllAuthors_clicked();

  void on_pushButtonUpdate_clicked();

  void on_pushButtonExit_clicked();

private:
  Ui::MainWindow *m_ui;
  GraphWidget *m_graph;
};
#endif // MAINWINDOW_H
