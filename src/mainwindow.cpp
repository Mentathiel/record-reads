#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "utils/Status.h"
#include "utils/graphics/renderer.h"

#include <QGraphicsRectItem>
#include <QGraphicsTextItem>
#include <QGraphicsView>
#include <QLayout>

#include <src/utils/graphics/graphwidget.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), m_ui(new Ui::MainWindow) {
  // General initialization
  m_ui->setupUi(this);
  m_graph = new GraphWidget();
  m_ui->graphicsWidget->layout()->addWidget(m_graph);
  //    // Creating scene for visual representation of books
  //    QGraphicsScene* scene = new QGraphicsScene();

  //    //Initializing scene renderer
  //    Renderer* m_renderer = new Renderer(scene);

  //    // Adding main window scene view
  //    QGraphicsView* sceneView = new QGraphicsView(scene);

  // Basic book drawing test
  //    Book testBook = Book({},"Autobiografija jednog
  //    matfovca",1997,true,Status::TO_READ);
  //    m_renderer->drawBookInScene(testBook);

  //    // Adding graphics view to main window
  //    QLayout* layout=this->layout();
  //    layout->addWidget(sceneView);
  //    this->setLayout(layout);
}

MainWindow::~MainWindow() { delete m_ui; }

void
MainWindow::SanityCheck(QGraphicsScene *_scene) {
  // Placeholder item for sanity test
  QGraphicsRectItem *rect = new QGraphicsRectItem();
  rect->setRect(0, 0, 400, 150);
  _scene->addItem(rect);
  return;
}

void
MainWindow::DrawBookInScene(Book &_book, QGraphicsScene *_scene) {
  // Extracting book info
  QString title = QString::fromStdString(_book.get_title());

  // Generating frame for book visual representation
  QGraphicsRectItem *frame = new QGraphicsRectItem();
  frame->setRect(0, 0, 300, 120);

  // Inserting text into graphics item
  QGraphicsTextItem *textItem = new QGraphicsTextItem(title, frame);

  // Adding item to scene
  _scene->addItem(frame);
  return;
}

void
MainWindow::on_pushButtonAddBook_clicked() {}

void
MainWindow::on_pushButtonRemoveBook_clicked() {}

void
MainWindow::on_pushButtonEditGenres_clicked() {}

void
MainWindow::on_pushButtonEditAuthors_clicked() {}

void
MainWindow::on_toolButtonGenreAdd_clicked() {}

void
MainWindow::on_toolButtonRemoveAllGenre_clicked() {}

void
MainWindow::on_toolButtonRemoveSelGenre_clicked() {}

void
MainWindow::on_toolButtonAuthorAdd_clicked() {}

void
MainWindow::on_toolButtonRemoveSelAuthors_clicked() {}

void
MainWindow::on_toolButtonRemoveAllAuthors_clicked() {}

void
MainWindow::on_pushButtonUpdate_clicked() {}

void
MainWindow::on_pushButtonExit_clicked() {}
