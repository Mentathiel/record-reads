# Record Reads

A desktop app that keeps track of one's book activity. It's a record of your reads. But it also reads records.

# About

<b>Written in:</b> C++

<b>Libraries:</b> Qt5

<b>Date:</b> April 2021

Faculty of Mathematics, University of Belgrade
